# Working with Scrum in the team

We use Trello for organizing scrum, one board for each sprint

## Sprints

We do sprint Planning as a group, collectively populating the backlog and deciding on prioritization. 
However, the project owner should ideally supply the main body of the backlog.
- A sprint should last 2-5 weeks
- Most sprints will coincide with major course deadlines:
    - Week 6 (5 Oct) : hand in project description
    - Week 9 (9 Nov) : demonstrate working simulator
    - week 13 (7 Dec) : present poster
    - ? (?) : hand in final report
- Additional sprints might be relevant, e.g. a second simulator sprint
- We try to only work on one sprint at the time
- In the first meeting after sprint Review, we start with a sprint retrospective before continuing with planning the next sprint.


## Meetings

We attempt to meet once a week outside the friday lecture, having an extented daily scrum loosely following this structure:
1. Discuss progress on tasks, and what each member plans to do this week
2. Discuss problems and challenges
3. (if relevant) review backlog and update prioritization of tasks

The scrum master is responsible for planning meetings.

## Trello Tasks 

Each board on Trello represent a sprint, and contains these lists:
- Backlog: wishlist for the project, items which has not yet been decided if should be done or not. Also a place to put ideas
- To-do: real specified tasks, which has been decided to be done and assigned one or more owner
- Doing: Task that is currently in the works
- Done: Completed task

Each card on the lists represent a task, which
- represents an amount of work which can be completed in maximum 2 weeks
- is assigned to one or more member, who are the responsible for completing the task
- has a "story point", signifying tasks workload: 
    - a number in the range 1-5 
    - written in parenthesis in the very beginning of the card title
    - (actual workload in terms of hours/days tbd)
- has a due-date 
- has a short description 

The scrum master is responsible for keeping everyone aligned on the tasks, making sure that is it clear who does what and when.
The scrum master also manages the Trello boards, making sure they are up-to-date.






