# Autonomous buses

The idea is to create a network of independent buses, that are not confined to one route, but instead communicate to get passengers from one place to another, as fast as possible.
Passengers can request a bus ride from a bus-stop ahead of time, and the buses network will make sure one bus will pick up the passenger and deliver them to their final destination.

The goal is to improve the bus-system by optimizing the waiting time, journey time etc.

We will look at this system in different scenarios and how well it performs in areas with different population densities and distribution.

# Minimal viable product

- Requirements
    - Visual simulation
    - Scenario generator
        - Maps are also generated
    - First-come first-served optimization
    - Event based simulation
- Limitations
    - Fixed number buses
    - Predetermined bus-stops
    - No traffic
    - All journey request are final
    - No malicious passengers
    - Rides are free
    - People behave the same as under the current system

# Modeling ideas

To best simulate passenger behavior we will use statistics from midttraffik, combined with simulations of different events. These events could be:
- Commuters
    - These people drive between the same two destinations every day, at specific times. All commuters drive at approximately the same time.
- Going to event 
    - These are large groups of people going from different places to the same place.
- Leaving event 
    - These are large groups of people leaving the same place, going to different places.

All these behaviors will be combined to create a scenario. A program will be made to generate these scenarios, using different parameters to test different combinations.

# Data

Statistics about number of passengers:
- [Midttraffik](https://www.midttrafik.dk/extranet/extranet-for-kommuner-og-region/undersogelser-og-statistik/passagertaellinger-i-midttrafik/)
- [NT](https://www.nordjyllandstrafikselskab.dk/Om-NT/Passagertaelling)
- [Fynbus](http://www.fynbus.dk/konomi-og-statistik)

