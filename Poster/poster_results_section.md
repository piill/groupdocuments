# Results

The graph shows how our system performs compared to real travel times gotten from RejsePlanen.

![alt text](simulation_results.png)

While the simulation uses a simple first-come-first-serve route planning method, the simulation can say to cover a "best case scenario" due to many simplifying assumptions:
- Busses travel directly between cities, with no stops
- Transit time estimated using car route planning on Google Maps
- Time spend picking up/drop of passengers not considered
- Passengers uniformly distributed throughout the day (from 06:00 to 22:00)
- Unlimited bus capacity

## Conclusion

The results suggests that using dynamic route planning, 5 busses could handle 1000 daily passengers on the same level as the current public bus system. Once a reasonable estimate of the current capacity/fleet of the area can be obtained, these results can give an initial idea of the real-life viability of the system.

## Further work

These steps could be taken in order to access the viability of the system:
- Estimating operating cost 
- Simulation of other scenarios, e.g. city center 
- Improved route planning algorithm
- Enabling pickup/dropoff at any location, not only bus stops