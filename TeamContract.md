# Contract

- We make decisions by consensus
- This contract can be changed, as long as all groupmembers agree to the change

## Tools

- We use markdown for documents
- We use Git and Gitlab.com for versioning and sharing documents
  - Trello is used for task overview, all specific code-tasks made as issues in GitLab. Refer to GitLab issues from Trello tasks.
  - Main branch is Master branch - no development branch
  - Create new branch for each issue
  - While working on an issue branch, continuously merge changes to master into the issue branch
  - When issue is closed, first merge master into issue branch, then merge into master - **remember to check that the code runs first!**
- We use Slack for communication and planning
- We use trello for project management and aim to implement scrum according to the guidelines described in [Scrum.md](Scrum.md) 
- We will decide on a programming language when we get closer
    - Alongside this, we will also agree to a styleguide

## Meetings

- We meet Monday from 10-13 & Friday 8:30-12
- We can change these times over Slack if needed
- We expect that people give notice the day before if they can't make it
- People are expected to work in between group meetings

 
## Conflict resolution

- If you are behind on an task, tell the group
- If you are late for or unable to participate in a meeting, tell the group
- If a member is not present for a meeting, the rest of the group will have to work around it, and you'll loose your say.
- It's the responsibility of the members to check Slack before meetings, and keep up to date
- If someone is not doing their part, the group will talk about it a meeting
    - If the problem continues, the group will have to continue the work, working around that groupmember. This will be made clear in the final report.

## Goals and expectation

- The weekly load outside of meetings is expected to be around 3-4 hours, however this may vary
- We expect everyone to keep up with the course material.
- The goal is make a good project, not just passing.
- We want to work systematically with the tools provided by course
    - No code is written before the group has agreed on a conceptual model
- We want to decide on a project that all in the group believes in, and that is approved by the lecturer.

## Team delegation

Scrum Master: Jesper

Product Owner: Pil

## Signatures

- Pil Eghoff (s164922)
- Charlie Goutorbe (s182352)
- Jesper Stald (s090031)
- Sveinbjörn Guðjohnsen (s182350)
